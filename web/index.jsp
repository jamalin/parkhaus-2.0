<%--
  Created by IntelliJ IDEA.
  User: Anita
  Date: 06.06.2019
  Time: 14:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
  <title>Welcome to CooleGarage</title>
</head>
<body>
<div>
  <h1>Parkhaus</h1>
  <a href="AdminServlet" class="button">Adminseite</a>
</div>

<div>
  <%-- Buttons usw. --%>
  Willkommen bei CooleGarage!
  Bitte wählen sie einen Parkplatztyp.

  <a href="CustomerServlet?type=0">Standardparkplatz</a>
  <a href="CustomerServlet?type=1">Behindertenparkplatz</a>
  <a href="CustomerServlet?type=2">Frauenparkplatz</a>
  <a href="CustomerServlet?type=3">Motorradstellplatz</a>

  <a href="CustomerServlet?type=help">Hilfe</a>
</div>

<div>
  <a href="CheckoutServlet" class="button">Kasse</a>
</div>

</body>
</html>

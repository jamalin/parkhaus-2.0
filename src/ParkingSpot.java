import java.time.Duration;
import java.time.LocalDateTime;


public class ParkingSpot {

    private LocalDateTime occupiedFrom;
    private String level;
    private String number;

    public ParkingSpot(int level, int number) {
        this.level = String.format("%02d", level);
        this.number = String.format("%03d", number);
    }

    public String occupy() {
        occupiedFrom = LocalDateTime.now();
        return getNumber();
    }

    public String getNumber() {
        return level + "." + number;
    }

    public long getOccupationTime() {
        Duration time = Duration.between(occupiedFrom, LocalDateTime.now());
        return time.getSeconds();
    }

}

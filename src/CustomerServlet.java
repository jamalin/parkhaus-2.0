import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(name = "CustomerServlet")
public class CustomerServlet extends HttpServlet {

    private CarParkStatus carParkStatus;

    public CustomerServlet() {
        carParkStatus = new CarParkStatus();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        if (request.getParameter("out") == true) {

        }

        int input = Integer.parseInt(request.getParameter("type"));

        response.setContentType("text/html");
        response.setCharacterEncoding("UTF-8");
        PrintWriter writer = response.getWriter();

        String[] outputParameters;

        switch (input) {
            case 0: {
                outputParameters = sellStandardParkingSpot();
                break;
            }
            case 1: {
                outputParameters = sellHandicappedParkingSpot();
                break;
            }
            case 2: {
                outputParameters = sellWomenParkingSpot();
                break;
            }
            case 3: {
                outputParameters = sellBikeParkingSpot();
                break;
            }
            default: {
                showErrorPage(writer);
                return;
            }
        }

        writer.println("<!DOCTYPE html><html>");
        writer.println("<head>");
        writer.println("<meta charset=\"UTF-8\" />");
        writer.println("<title> Zuweisung " + outputParameters[1] + "</title>");
        writer.println("</head>");
        writer.println("<body>");
        writer.println("<h1> Ihnen wurde der " + outputParameters[1] + " mit der Nummer: " + outputParameters[0] + " zugewiesen. </h1>");
        writer.println("</body>");
        writer.println("</html>");

    }

    private String[] sellStandardParkingSpot() {
        String[] out = new String[3];
        out[0] = carParkStatus.occupyStandardSpot();
        out[1] = "Parkplatz";
        return out;
    }

    private String[] sellHandicappedParkingSpot() {
        String[] out = new String[3];
        out[0] = carParkStatus.occupyHandicappedSpot();
        out[1] = "Behindertenparkplatz";
        return out;
    }

    private String[] sellWomenParkingSpot() {
        String[] out = new String[3];
        out[0] = carParkStatus.occupyWomenSpot();
        out[1] = "Frauenparkpletz";
        return out;
    }

    private String[] sellBikeParkingSpot() {
        String[] out = new String[3];
        out[0] = carParkStatus.occupyBikeSpot();
        out[1] = "Motorradstellplatz";
        return out;
    }

    private void showErrorPage(PrintWriter writer) {
        writer.println("<!DOCTYPE html><html>");
        writer.println("<head>");
        writer.println("<meta charset=\"UTF-8\" />");
        writer.println("<title> Huch! </title>");
        writer.println("</head>");
        writer.println("<body>");
        writer.println("<h1> Ein ungültiger Get-Parameter wurde gefunden. Das hätte nicht passieren sollen. </h1>");
        writer.println("</body>");
        writer.println("</html>");
    }

}

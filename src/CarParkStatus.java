import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Optional;

public class CarParkStatus {

//Parameters
    //standardSpots parking spots
    private ParkingSpot[] standardSpots;
    private int freeStandardSpots;

    //handicappedSpots parking spots
    private ParkingSpot[] handicappedSpots;
    private int freeHandicappedSpots;

    //womenSpots parking spots
    private ParkingSpot[] womenSpots;
    private int freeWomenSpots;

    //bike parking spots
    private ParkingSpot[] bikeSpots;
    private int freeBikeSpots;


//Setters
    public void setStandardSpots(ParkingSpot[] standardSpots) {
        this.standardSpots = standardSpots;
        this.freeStandardSpots = standardSpots.length;
    }

    public void setHandicappedSpots(ParkingSpot[] handicappedSpots) {
        this.handicappedSpots = handicappedSpots;
        this.freeStandardSpots = handicappedSpots.length;
    }

    public void setWomenSpots(ParkingSpot[] womenSpots) {
        this.womenSpots = womenSpots;
        this.freeStandardSpots = womenSpots.length;
    }

    public void setBikeSpots(ParkingSpot[] bikeSpots) {
        this.bikeSpots = bikeSpots;
        this.freeStandardSpots = bikeSpots.length;
    }


//Members
    public String occupyStandardSpot() {
        return standardSpots[standardSpots.length - freeStandardSpots--].occupy();
    }

    public String occupyHandicappedSpot() {
        return handicappedSpots[handicappedSpots.length - freeHandicappedSpots--].occupy();
    }

    public String occupyWomenSpot() {
        return womenSpots[womenSpots.length - freeWomenSpots--].occupy();
    }

    public String occupyBikeSpot() {
        return bikeSpots[bikeSpots.length - freeBikeSpots--].occupy();
    }

    public ParkingSpot getSpot(String number) {

        Optional[] optionals = new Optional[4];
        optionals[0] = Arrays.stream(standardSpots).filter(s -> s.getNumber().equals(number)).findFirst();
        optionals[1] = Arrays.stream(handicappedSpots).filter(s -> s.getNumber().equals(number)).findFirst();
        optionals[2] = Arrays.stream(womenSpots).filter(s -> s.getNumber().equals(number)).findFirst();
        optionals[3] = Arrays.stream(bikeSpots).filter(s -> s.getNumber().equals(number)).findFirst();

        for (Optional optional : optionals) {
            if (optional.isPresent()) {
                return (ParkingSpot) optional.get();
            }
        }

        throw new NoSuchElementException("No Spot with that Number found");
    }

}
